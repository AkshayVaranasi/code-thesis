% This file reconstruct a image using the regular multilevel random sampling 
% schme and a uniformly random sampling scheme.

clear all
filename = 'images/birds_1024.png';
subsamplingRate = 0.20;             % M = round(N*subsamplingRate)
vm    = 4;                          % Number of vanishing moments
sigma = 100;                        % Minimize ||Ax-b||_2 subject to ||x||_1 ≤ sigma
ft_rect    = FT_Hadamard2d_rect;         % Choose fast transform
ft_unif    = FT_Hadamard2d_uniform;         % Choose fast transform

% Read image and convert it to a vector
im = double(imread(filename));
[im_rows, im_cols] = size(im);
N = im_rows*im_cols;
M = round(N*subsamplingRate);

ft_rect.M = M;
ft_rect.N = N;
ft_rect.rows = im_rows;
ft_rect.cols = im_cols;
ft_rect.vm = vm;
ft_rect.nres = floor(log2(im_rows) + 1e-8);

ft_unif.M = M;
ft_unif.N = N;
ft_unif.rows = im_rows;
ft_unif.cols = im_cols;
ft_unif.vm = vm;
ft_unif.nres = floor(log2(im_rows) + 1e-8);


liftingfactortho(vm);

ft_rect.create_sampling_scheme();
ft_unif.create_sampling_scheme();

b_rect = ft_rect.sampling_operator(im);  
b_unif = ft_unif.sampling_operator(im);  
 
opA_rect = @(x, mode) ft_rect.sampling_and_inverse_wavelet_operator(x, mode);
opA_unif = @(x, mode) ft_unif.sampling_and_inverse_wavelet_operator(x, mode);
 
opts = spgSetParms('verbosity',1);
z_rect = spg_bpdn(opA_rect, b_rect, sigma, opts); %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma
z_unif = spg_bpdn(opA_unif, b_unif, sigma, opts); %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma

% Reconstruct image 
z_rect = reshape(z_rect, im_rows, im_cols);
im_rec_rect = IDWT2Impl(z_rect, ft_rect.nres, @IDWTKernelOrtho);

z_unif = reshape(z_unif, im_rows, im_cols);
im_rec_unif = IDWT2Impl(z_unif, ft_unif.nres, @IDWTKernelOrtho);


if  (~exist('rec_cnt'))
    rec_cnt = 1;
end

if ft_rect.is_fourier
    opStr = 'fourier';
else 
    opStr = 'hadam';
end

filename_rec_rect = sprintf('plots/rec%d_subs_%d_sig_%d_op_%s_rect.png', ...
                            rec_cnt, 100*subsamplingRate, sigma, opStr);
imwrite(uint8(im_rec_rect),  filename_rec_rect);
fprintf('Saving image as %s\n', filename_rec_rect);

filename_rec_unif = sprintf('plots/rec%d_subs_%d_sig_%d_op_%s_unif.png', ...
                            rec_cnt, 100*subsamplingRate, sigma, opStr);
imwrite(uint8(im_rec_unif),  filename_rec_unif);
fprintf('Saving image as %s\n', filename_rec_unif);


% Save sampling pattern
Y = zeros(ft_rect.rows,ft_rect.cols);
Y(ft_rect.idx) = 255;
filename_samples_rect = sprintf('plots/sp_rec%d_%s_%d_rect.png', ...
                                rec_cnt, opStr, ft_rect.rows);
imwrite(uint8(Y), filename_samples_rect);
fprintf('Saving sampling pattern as %s\n', filename_samples_rect);

% Save sampling pattern
Y = zeros(ft_unif.rows,ft_unif.cols);
Y(ft_unif.idx) = 255;
filename_samples_unif = sprintf('plots/sp_rec%d_%s_%d_unif.png', ... 
                                    rec_cnt, opStr,ft_unif.rows);
imwrite(uint8(Y), filename_samples_unif);
fprintf('Saving sampling pattern as %s\n', filename_samples_rect);



rec_cnt = rec_cnt + 1;



