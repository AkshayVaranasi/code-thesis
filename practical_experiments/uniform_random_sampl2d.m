% Creates a sampling pattern where all samples are drawn uniformly at random.
% It does not possess any structure. 
function pos = uniform_random_sampl2d(im_rows, im_cols, nbr_samples)
    
    Y = zeros(im_rows, im_cols);
    pos = zeros(nbr_samples, 2);
    s = 1;
    while (s <= nbr_samples)
        i = round(1 + (im_rows-1)*rand(1));
        j = round(1 + (im_cols-1)*rand(1));
        if (~Y(i,j))
            pos(s,:) = [i, j];
            s = s + 1;
            Y(i,j) = 1;
        end
    end
    
end

