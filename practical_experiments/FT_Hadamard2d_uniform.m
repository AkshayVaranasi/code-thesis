
classdef FT_Hadamard2d_uniform < Fast_transform_general2d 
    
    properties
        hadamard_order = 'sequency';
        is_fourier = 0;
    end
    
     
    methods 
         
        function y = sampling_and_inverse_wavelet_operator(obj, x, mode);
            y = had2IDB2d(x, mode, obj.idx, obj.nres, obj.rows, obj.hadamard_order);
        end  
         
        function b = sampling_operator(obj, x) 
            b = fastwht2d(x, obj.hadamard_order)*obj.rows;
            b = b(obj.idx);
        end
         
        function obj = create_sampling_scheme(obj) 
            
            % Test that all necessary variables have been initialized
            Fast_transform_general.is_initialized(obj.M, 'M is not initialized'); 
            Fast_transform_general.is_initialized(obj.cols, 'cols is not initialized'); 
            Fast_transform_general.is_initialized(obj.rows, 'rows is not initialized'); 
            

            % Pick samples uniformly at random 
            pos = uniform_random_sampl2d(obj.rows, obj.cols, obj.M);
            
            % Convert to sampling indices. 
            obj.idx = sub2ind([obj.rows, obj.cols], pos(:,1), pos(:,2));
              
        end  
         
    end  
     
end



