

% This code reads an image using the scales 256, 512, 1024 and 2048. Samples 
% this image using fixed sampling rate, and reconstruct the image with the 
% SPGL1 algorithm. 


for rows = 2.^(8:11)
    filename = sprintf('pictures/klubbe%d.png', rows);
    subsamplingRate = 0.05;             % M = round(N*subsamplingRate)
    vm    = 4;                          % Number of vanishing moments
    sigma = 100;                        % Minimize ||Ax-b||_2 subject to ||x||_1 ≤ sigma
    ft    = FT_Fourier2d_circ;          % Choose fast transform
    hadamardOrder = 'sequency';
    
    % Read image and convert it to a vector
    im = double(imread(filename));
    [im_rows, im_cols] = size(im);
    N = im_rows*im_cols;
    M = round(N*subsamplingRate);
    
    ft.M = M;
    ft.N = N;
    ft.rows = im_rows;
    ft.cols = im_cols;
    ft.vm = vm;
    ft.nres = floor(log2(im_rows) + 1e-8);
    
    liftingfactortho(vm);
    
    ft.create_sampling_scheme();
    
    b = ft.sampling_operator(im);  
    
    opA = @(x, mode) ft.sampling_and_inverse_wavelet_operator(x, mode);
    
    opts = spgSetParms('verbosity',1);
    z = spg_bpdn(opA, b, sigma, opts); %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma
    
    % Reconstruct image 
    z1 = reshape(z, im_rows, im_cols);
     
    im_rec = IDWT2Impl(z1, ft.nres, @IDWTKernelOrtho);
     
     
    %im_rec = 255*(im_rec - min(min(im_rec)))/max((max(im_rec)) - max(min(im_rec)));
    %im_rec = uint8(reshape(im_rec, im_rows, im_cols));
     
    if  (~exist('rec_cnt'))
        rec_cnt = 1;
    end
    
    if ft.is_fourier
        opStr = 'fourier';
    else 
        opStr = 'hadam';
    end
    
    
    filename_rec = sprintf('plots/rec%d_dim_%d_subs_%d_sig_%d_op_%s_asym.png', ...
                   rec_cnt, ft.rows, round(100*subsamplingRate), sigma, opStr);
    imwrite(uint8(im_rec),  filename_rec);
    fprintf('Saving image as %s\n', filename_rec);
    
    % Save sampling pattern
    Y = zeros(ft.rows,ft.cols);
    Y(ft.idx) = 255;
    filename_samples = sprintf('plots/sp_rec%d_%s_dim_%d_subs_%d_asym.png',...
                       rec_cnt, opStr, ft.rows,round(100*subsamplingRate));
    imwrite(uint8(Y), filename_samples);
    fprintf('Saving sampling pattern as %s\n', filename_samples);

end

rec_cnt = rec_cnt + 1;




