%
% FT_Fourier2d_circ is a two dimensional Fourier transform which can be applied
% to images of size 2^R × 2^R. It uses a circular sampling scheme emphasizing 
% fully sampling all frequencies below 2^{R-4} and uses uniformly subsampling 
% of frequencies between (2^{R-4}, 2^{R-2}] and (2^{R-2}, 2^{R-1}].
%
classdef FT_Fourier2d_circ < Fast_transform_general2d 
    
    properties
        is_fourier = 1;
    end
     
    methods 
         
        function y = sampling_and_inverse_wavelet_operator(obj, x, mode);
            y = fourier2IDB2d(x, mode, obj.idx, obj.nres, obj.rows);
        end  
         
        function b = sampling_operator(obj, x) 
            b = fftshift(fft2(x))/obj.rows;
            b = b(obj.idx);
        end
         
        function obj = create_sampling_scheme(obj) 
            
            % Test that all necessary variables have been initialized
            Fast_transform_general.is_initialized(obj.M, 'M is not initialized'); 
            Fast_transform_general.is_initialized(obj.cols, 'cols is not initialized'); 
            Fast_transform_general.is_initialized(obj.rows, 'rows is not initialized'); 
            
            nu = round(log2(obj.cols));
            
            include_border = 0; % Boolean

            % Radius of each circle 
            bounds = [2^(nu-4), 2^(nu-2), 2^(nu-1)];
             
            % Pick samples uniformly at random within each bin
            pos = random_circ_subsamp(obj.rows, obj.cols, bounds, obj.M, 1, include_border);
            
            % Convert to sampling indices. 
            obj.idx = sub2ind([obj.rows, obj.cols], pos(:,1), pos(:,2));
            
        end  
         
    end  
     
end


