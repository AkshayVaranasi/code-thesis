% This code will sample a image with the given sampling operator and recover it 
% from these samples using the SPGL1 algorithm. 

%clear all
filename = 'pictures/klubbe1024.png';
subsamplingRate = 0.25;             % M = round(N*subsamplingRate)
vm    = 4;                          % Number of vanishing moments
sigma = 100;                        % Minimize ||Ax-b||_2 subject to ||x||_1 ≤ sigma
ft    = FT_Hadamard2d_rect;         % Choose fast transform

hadamardOrder = 'sequency'; % not currently in use

% Read image and convert it to a vector
im = double(imread(filename));
[im_rows, im_cols] = size(im);
N = im_rows*im_cols;
M = round(N*subsamplingRate);

ft.M = M;
ft.N = N;
ft.rows = im_rows;
ft.cols = im_cols;
ft.vm = vm;
ft.nres = floor(log2(im_rows) + 1e-8);

liftingfactortho(vm);

ft.create_sampling_scheme();

b = ft.sampling_operator(im);  
 
opA = @(x, mode) ft.sampling_and_inverse_wavelet_operator(x, mode);
 
opts = spgSetParms('verbosity',1);
z    = spg_bpdn(opA, b, sigma, opts); %  minimize ||x||_1  s.t.  ||Ax - b||_2 <= sigma

% Reconstruct image 
z      = reshape(z, im_rows, im_cols);
im_rec = IDWT2Impl(z, ft.nres, @IDWTKernelOrtho);


if  (~exist('rec_cnt'))
    rec_cnt = 1;
end
if ft.is_fourier
    opStr = 'fourier';
else 
    opStr = 'hadam';
end

filename_rec = sprintf('plots/rec%d_subs_%d_sig_%d_op_%s.png', ...
                            rec_cnt, 100*subsamplingRate, sigma, opStr);

imwrite(uint8(im_rec),  filename_rec);
fprintf('Saving image as %s\n', filename_rec);

% Save sampling pattern
Y = zeros(ft.rows,ft.cols);
Y(ft.idx) = 255;
filename_samples = sprintf('plots/sp_rec%d_%s_%d.png', rec_cnt, opStr, ft.rows);
imwrite(uint8(Y), filename_samples);
fprintf('Saving sampling pattern as %s\n', filename_samples);

rec_cnt = rec_cnt + 1;





