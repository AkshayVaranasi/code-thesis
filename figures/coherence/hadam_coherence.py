from numpy import *
from hadamard import fastwht
from dwt import DWTImpl, IDWTImpl, DWTKernelOrtho, IDWTKernelOrtho, DWTKernelHaar, IDWTKernelHaar, liftingfactortho;
import matplotlib.pyplot as plt;
import matplotlib.cm as cm;
from mpl_toolkits.axes_grid1 import make_axes_locatable;
import os;
import sys;

"""
Multiplies the Hadamard matrix with a wavelet matrix and writes the maximum
element in each row and column to a file.
"""

fsize = 25;
set_printoptions(linewidth=120);
hadamard_order = 'sequency';
dest_data = 'data/';
dest_plot = 'plots/'
vm = 2;
nu = 10;
dest = 'plots/';
nres = nu;
N    = 2**nu;

#os.system("rm -f %sdetail*" % dest);


liftingfactortho(vm);

max_row = zeros(N);
max_col = zeros(N);


for i in xrange(N):
    ei = zeros(N); ei[i]=1;
     
    IDWTImpl(ei, nres, IDWTKernelOrtho,False); 
    ei = fastwht(ei, order=hadamard_order);
    ei *= sqrt(N);
    ei = abs(ei);
    max_elem = amax(ei);
    max_col[i] = max_elem;
    
    idx = ei > max_row;
    max_row[idx] = ei[idx];
    

idx = zeros(N);
n=0;
while(n < nu):
    idx[2**n-1:2**(n+1)] = 2.0**(-n+1);
    n += 1;


outfile = open(dest_data + "hadamard_DB_%d_dim_%d.txt" % (vm, nu), 'w');

for i in xrange(N):
    outfile.write("%19.16e %19.16e\n" % (max_col[i], max_row[i]));
    
outfile.close();



