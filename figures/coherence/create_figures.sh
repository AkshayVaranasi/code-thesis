#! /bin/sh
echo "Starting coherence plots"
python2 read_file_coherence.py 2 16 0
python2 read_file_coherence.py 2 16 1
python2 read_file_coherence.py 3 16 0
python2 read_file_coherence.py 3 16 1

echo "Starting max_elem plots"
python2 read_file_max_elem.py 2 16 0
python2 read_file_max_elem.py 2 16 1
python2 read_file_max_elem.py 3 16 0
python2 read_file_max_elem.py 3 16 1



