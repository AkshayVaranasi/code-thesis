% This simple script test whether or not the boundary wavelets work.  

vm = 2;
nu = 4;
N  = 2^nu;

A = [.3248940464, 0; .03715799299, 1.001445417];


% Creating a linear polynomial
x  = linspace(1,N,N)';
%wc = FWT_CDJV_no_precon(x, 3, vm);
%y = IWT_CDJV_no_postcon(wc,3,vm);

wc = FWT_CDJV_fixed(x, 3, vm);
y = IWT_CDJV_fixed(wc,3,vm);


%idx = 511
%N = 2^10;
%ei = zeros(N,1);
%ei(511) = 1;
%wc1 = IWT_CDJV_no_postcon(ei,3,vm);
%wc2 = IWT_CDJV(ei,3,vm);
%figure();
%plot(wc1(end-20:end))
%title('No postcon');
%
%figure();
%plot(wc2())
%title('With postcon');



