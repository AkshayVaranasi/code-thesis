% This file is used to generate the values of the boundary wavelet functions. 
% These are then written to file, so that waveplot.py can plot them.


dest_data = 'data/';
vm = 2;
V_0 = 3;
nu = 10;
N  = 2^nu;

for vm = [2,3]
    
    for k = 1:vm
        % LEFT WAVELET
        x = zeros(N,1);
        x(2^V_0+k) = 2^((nu-V_0)/2);
        
        % Find wavelet values
        y = IWT_CDJV_no_postcon(x, V_0, vm);
        
        % Write values to file
        filename = sprintf('%sDB%d_left_%d.txt', dest_data, vm,k);
        fileID = fopen(filename,'w');
        for i = 1:round(((vm+k-1)/2^V_0)*N)
            fprintf(fileID, '%19.16e\n', y(i));
        end
        fclose(fileID);
        
        % RIGHT WAVELET
        x = zeros(N,1);
        x(2^(V_0+1)-k+1) = 2^((nu-V_0)/2);
        
        % Find wavelet values
        y = IWT_CDJV_no_postcon(x, V_0, vm);
        
        % Write values to file
        filename = sprintf('%sDB%d_right_%d.txt', dest_data, vm,k);
        fileID = fopen(filename,'w');
        % Start here
        n = round((1 -(vm+k-1)/2^V_0)*N)+1;
        for i = n:N
            fprintf(fileID, '%19.16e\n', y(i));
        end
        fclose(fileID);
        figure();
        plot(y(n:N));
        title(sprintf('DB%dRight%d',vm,k));
    end
end






