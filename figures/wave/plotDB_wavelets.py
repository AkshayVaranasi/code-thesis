from numpy import *;
import matplotlib.pyplot as plt;

"""
Plots the phi and psi functions for all vanishing momemnts from 2 to 10.
The data to the phi and psi functions have been generated from my C++ wavelet libary. 
"""



src = "data/";
dest = 'plots/';
fsize = 25;
lwidth = 3;
my_dpi = 150;


vm = 2;

for vm in range(2,11):

    N = (2*vm-1)*(2**10)
    
    x_psi = zeros(N);
    x_phi = zeros(N);
    
    infile_phi = open(src + "DB_scaling_%d_.txt" % vm);
    infile_psi = open(src + "DB_%d_.txt" % vm);
    
    i = 0;
    for line in infile_psi:
        x_psi[i] = float(line);
        i += 1;
        
    i = 0;
    for line in infile_phi:
        x_phi[i] = float(line);
        i += 1;
        
    infile_psi.close();
    infile_phi.close();
    
    
    t_psi = linspace(-vm+1, vm, N); 
    plt.figure();
    plt.plot(t_psi,x_psi, 'k', linewidth=lwidth);
    plt.xticks(range(-vm+1, vm+1), fontsize=fsize);
    plt.yticks(fontsize=fsize);
    plt.title("$\psi$", fontsize=fsize+15, y=1.02);
    plt.xlabel(r"$\nu = %d$" % vm, fontsize=fsize+15);
    plt.savefig(dest + 'psi%d.png' %vm, dpi=my_dpi, bbox_inches='tight'); 
    
    t_phi = linspace(0, 2*vm-1, N); 
    plt.figure();
    plt.plot(t_phi,x_phi, 'k', linewidth=lwidth);
    plt.xticks(range(0, 2*vm), fontsize=fsize);
    plt.yticks(fontsize=fsize);
    plt.title("$\phi$", fontsize=fsize+15,y=1.02);
    plt.savefig(dest + 'phi%d.png' %vm, dpi=my_dpi, bbox_inches='tight'); 




