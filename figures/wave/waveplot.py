from numpy import *;
import matplotlib.pyplot as plt;

'''
This file plot the boundary wavelet from the data produced by waveplot.m
'''


src = "data/";
dest = 'plots/';
fsize = 25;
lwidth = 3;
my_dpi = 150;

for vm in [2, 3]:
    
    for k in range(vm):
        N = round(1024*(vm+k)/8.0)
        x_left  = zeros(N);
        x_right = zeros(N);
    
        infile_left  = open(src + "DB%d_left_%d.txt" % (vm, k+1));
        infile_right = open(src + "DB%d_right_%d.txt" % (vm,k+1));
    
        i = 0;
        #print 'left:  %d, %d' % (k+1, vm)
        for line in infile_left:
            x_left[i] = float(line);
            i += 1;
        if (i != N):
            print 'left_%d:  i = %d, N = %d' %(k+1,i,N);
        
        #print 'right:  %d, %d' % (k+1, vm)
        i = 0;
        for line in infile_right:
            x_right[i] = float(line);
            i += 1;
        if (i != N):
            print 'rigth_%d: i = %d, N = %d' %(k+1,i,N);
        infile_left.close();
        infile_right.close();
    
    
        t_left = linspace(0, vm+k, N); 
        plt.figure();
        plt.plot(t_left,x_left, 'k', linewidth=lwidth);
        plt.xticks(range(0,vm+k+1), fontsize=fsize);
        plt.yticks(fontsize=fsize);
        plt.title("$\psi^{left}_{%d}$" % (k+1), fontsize=fsize+15, y=1.02);
        #plt.xlabel(r"$\nu = %d$" % vm, fontsize=fsize+15);
        plt.savefig(dest + 'DB%d_left_%d.png' %(vm, k+1), dpi=my_dpi, bbox_inches='tight'); 
        
        t_right = linspace(-(vm+k), 0, N); 
        plt.figure();
        plt.plot(t_right,x_right, 'k', linewidth=lwidth);
        plt.xticks(range(-(vm+k),1), fontsize=fsize);
        plt.yticks(fontsize=fsize);
        plt.title("$\psi_{%d}^{right}$" % (k+1), fontsize=fsize+15, y=1.02);
        plt.savefig(dest + 'DB%d_right_%d.png' % (vm, k+1), dpi=my_dpi,  bbox_inches='tight'); 







