from numpy import *;
from hadamard import fastwht;
from dwt import DWTImpl, IDWTImpl, DWTKernelOrtho, IDWTKernelOrtho, DWTKernelHaar, IDWTKernelHaar, liftingfactortho;
import matplotlib.pyplot as plt;
import os;

"""
This file produce a tikz code which will draw the first 16 Walsh functions. 
"""



set_printoptions(linewidth=170, precision=5);

def seqmat(N):
    """
    Return the sequency Hadamard matrix as a two dimensional array
    """
    nu = int(round(log2(N))); 
    if (N != 2**nu):
        print "Error: %d is non-dyadic" % (N);
        N = 2**nu;
    U = zeros([N,N]);
    for i in xrange(N):
        ei = zeros(N);
        ei[i] = 1;
        ei = N*fastwht(ei);
        U[:,i] = ei;
    return U;

def palmat(N):
    """
    Return the sequency Hadamard matrix as a two dimensional array
    """
    nu = int(round(log2(N))); 
    if (N != 2**nu):
        print "Error: %d is non-dyadic" % (N);
        N = 2**nu;
    U = zeros([N,N]);
    for i in xrange(N):
        ei = zeros(N);
        ei[i] = 1;
        ei = N*fastwht(ei,order='dyadic');
        U[:,i] = ei;
    return U;

def signChanges(x):
    """
    This function does only work for {-1, 1} arrays
    """
    
    N = len(x);
    signChange = 0;
    for i in range(N-1):
        if( x[i] != x[i+1]):
            signChange += 1;
    return signChange;

def createTikzString(x, had_height, width, y, numb, paley):
    N = len(x);
    had_width = width/float(N);
    walFreq = signChanges(x);
    cur_width = 0;
    drawline = r"\draw[very thick]  (%g, %g) -- (%g, %g)"  % (cur_width, y, cur_width + had_width, y);
    cur_width += had_width;
    for i in xrange(1, N):
        cur_width += had_width;
        if x[i-1] == x[i]: 
            tmp_line = " -- (%g, %g) " % (cur_width, (-0.5*(1 - x[i])*had_width + y));
        else:
            tmp_line = " -- (%g, %g) -- (%g, %g)" % (cur_width-had_width, -0.5*(1 - x[i])*had_width + y, cur_width, -0.5*(1 - x[i])*had_width + y);
            
        drawline += tmp_line;
    if (paley):
        drawline += r""";
        \draw (-1, %g) node {$\text{PAL}(%d, x)$};
        \draw (%g, %g) node[right]  {$\text{WAL}(%d, x)$};
        """ % (y, numb, width, y, walFreq);
    else:
        drawline += r""";
        \draw (-1, %g) node {WAL(%d, T)};
        """ % (y, numb);
    return drawline;


if __name__ == "__main__":
    
    dest = '../tikzimages/'
    paley = True;
    
    nu = 4;
    vm = 2;
    nres = nu;
    N = 2**nu;
    height = 17;
    width = 10;
    vspace_percent = 20; # % 
    had_height = (height*(100-vspace_percent)/100.0)/float(N);
    vspace = (height*vspace_percent/100.0)/float(N-1);
    
    if (paley):
        filename = dest + "paley%d.tex" % (N); 
        #print "Created file: %s" % filename;
    else:
        filename = dest + "hadam%d.tex" % (N);
    
    os.system("rm -f %s" % filename);
    outfile = open(filename, 'w');
    
    outfile.write(r"""
    
    \begin{tikzpicture}[scale=1]
    """);
    
    if (paley):
        had = palmat(N);
    else:
        had = seqmat(N);
    y = height;
    for i in xrange(N):
        drawline = createTikzString(had[i, :], had_height, width, y, i, paley);
        y -= had_height + vspace;
        outfile.write(drawline);
    
    
    outfile.write(r"""
    \end{tikzpicture}
    """);
    
    outfile.close();

