from numpy import *
from hadamard import fastwht
from dwt import DWTImpl, IDWTImpl, DWTKernelOrtho, IDWTKernelOrtho, DWTKernelHaar, IDWTKernelHaar, liftingfactortho;
import matplotlib.pyplot as plt;
import matplotlib.cm as cm;
from mpl_toolkits.axes_grid1 import make_axes_locatable;
import os;
import sys;

"""
Plots the Hadamard matrix multiplied by the IDWT matrix. The order of the 
Hadamard matrix and the number of vanishing moments for the Daubechies wavelet
is decided as command line arguments. 
"""



fsize = 25;
set_printoptions(linewidth=120);

vm = 1;
try:
    vm = int(sys.argv[1]);
except:
    print "Pleas provide the number of vanishing moments as a cmd line argument"

hadamard_order = 'sequency';
try:
    hadamard_order = sys.argv[2];
except:
    a = 1; # Do nothing
if hadamard_order not in ['sequency', 'hadamard', 'dyadic']:
    hadamard_order = 'sequency';



nu = 10;

dest = 'plots/';
nres = nu;
N    = 2**nu;
#os.system("rm -f %sdetail*" % dest);

block_nbr = 2;
K = 2**block_nbr;

liftingfactortho(vm);

U = zeros([N, N]);
for i in xrange(N):
    ei = zeros(N); ei[i]=1;
     
    IDWTImpl(ei, nres, IDWTKernelOrtho,False); 
    ei = fastwht(ei, order=hadamard_order);
    ei *= sqrt(N);
    U[:,i] =  ei;

plt.figure();    
ax = plt.gca();
im = ax.matshow(abs(U), cmap=cm.jet);
plt.xticks([]);
plt.yticks([]);
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cbar = plt.colorbar(im, cax=cax);
cbar.set_ticks([0,0.25,0.5,0.75,1]);
cbar.set_ticklabels([0,0.25,0.5,0.75,1]);
cbar.ax.tick_params(labelsize=fsize);
#plt.show();
plt.savefig("%s/hadamard_DB_%d_%s.png" % (dest, vm, hadamard_order), dpi=150, \
            transparent=False, bbox_inches='tight');






