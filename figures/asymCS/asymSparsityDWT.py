from numpy import *
import matplotlib.pyplot as plt
from dwt import DWTImpl, IDWTImpl, DWT2Impl, IDWT2Impl, DWTKernelOrtho, IDWTKernelOrtho, DWTKernelHaar, IDWTKernelHaar, liftingfactortho
import sys


"""
This file produce the asymptotic sparsity plots using the `Local sparsity` as a 
measure of the sparsity.
"""

def local_sparsity(x, eps): 
    
    N = len(x);
    x = asarray(x);
    norm_x = linalg.norm(x,ord=2);
    signal_cum = cumsum(abs(x));
    

    s = 0;
    sq_sum = 0;
    while (sqrt(sq_sum) <= eps*norm_x and s < N): 
        sq_sum += x[s]*x[s];
        s += 1;
    #L = sum(signal_cum <= eps*signal_cum[-1]); 
    return s

def sparsify_vector_DWT(x, keepFraction, nres):
    
    N = len(x);
    

if __name__ == "__main__":
    vm = 8;
    try:
        vm = int(sys.argv[1]);
    except:
        print "Pleas provide the number of vanishing moments as a cmd line argument"
        #
    filename = 'klubbe1024.png'
    dest = 'plots/'
    fsize = 26; 
    nbr_of_levels = 7;
     
    liftingfactortho(vm);
     
    f = asarray(plt.imread(filename),dtype=float64);
     
    f *= 255;
     
    M, N = f.shape; 
    assert M==N, 'Image must be quadratic';
    
    #f.shape = (M*N,1);
    n = int(floor(log2(N)+1e-5));
     
    K = N*M;
    
    nres =  int(floor(log2(N)+1e-6))-2;
     
    DWT2Impl(f, nres, DWTKernelOrtho, False); 
     
    all_level_arrays = [];
    level_nbr = range(n-1,n-nbr_of_levels-1, -1); 
    K_itr = N;
     
    s = 0; 
    for i in range(nbr_of_levels):
         
        local_array = f[K_itr/2:K_itr,:K_itr].copy();
        #print 'i: %d local_array.shape = ' % (i), local_array.shape;
        local_array = reshape(local_array, -1); # make one dimensional
        local_array2 = f[0:K_itr/2, K_itr/2:K_itr].copy();
        #print 'i: %d local_array2.shape = ' % (i), local_array2.shape;
        local_array2 = reshape(local_array2,-1);
        
        local_array3 = concatenate((local_array,local_array2)); 
        #print 'i: %d local_array3.shape = ' % (i), local_array3.shape;
         
        tmp = abs(sort(-abs(local_array3), axis=0)); 
        all_level_arrays.append(tmp);
        K_itr = K_itr/2;
        
    N_eps = 101;
    eps = linspace(0,100,N_eps)/100; # eps = 0.01, 0.02, ..., 1
    eps[0] = 1e-5; 
    all_local_sparsities = zeros([nbr_of_levels, N_eps]);
     
    for i in range(N_eps):
        for k in range(nbr_of_levels):
            all_local_sparsities[k,i] = local_sparsity(all_level_arrays[k], eps[i]);
    
    #for k in range(nbr_of_levels):
    #    print all_level_arrays[k].shape;
    
     
    
    #print "Mean: ", mean(all_level_arrays[1]);
    plt.figure();
    plt.hold('on');
    leg = [];
    for k in range(nbr_of_levels): 
        level_size = 3*(2**(2*(level_nbr[k])));
        #print 'k: %d, L: %d, level_size: %d, level_nbr[k]: %d'  \
        #            %(k, all_local_sparsities[k,-1],level_size, level_nbr[k]);
        
        plt.plot(eps, all_local_sparsities[k,:]/float(level_size),linewidth=2);
        leg.append('$W_{%d}$' % level_nbr[k]);
    plt.xlabel("$\epsilon$", fontsize=fsize+10);
    plt.ylabel(r"$s_{k}(\epsilon)/(M_{k} - M_{k-1})$",fontsize=fsize+10);
    plt.xticks(fontsize=fsize);
    plt.yticks(fontsize=fsize);
    plt.legend(leg, bbox_to_anchor=(0.35, 1),fontsize=fsize);
    plt.savefig(dest+"asymptotic_sparsity_DWT_DB_%d.png" % (vm),  dpi=150, \
                transparent = False, bbox_inches = 'tight');
        
    










