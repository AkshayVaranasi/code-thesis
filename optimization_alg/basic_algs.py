from numpy import *;
import sys;

######################### Create random matrices ############################

def create_sampling_mat(m,N):
    """
    Creates an m x N matrix with elements drawn uniformly from the set 
    {-1/sqrt(m), 1/sqrt(m)}. Hence it only contains these two elements.  
    """
    A = random.randint(0,2, [m,N]);
    A = (1/sqrt(m))*(A - (A==0));
    return matrix(A).astype(float);


def rand_spars_vec(N, sparsity, factor=1, b=0):
    """
    Create a column vector of length N, with 'sparsity' number of non-zero 
    coefficients. Numbers are drawn uniformly from the interval [0,1).
    By specifying a factor and a 'b'-value, we can transform the this interval
    by the linear transform 'factor*x + b'.    
    """
    x = matrix(zeros([N,1]));
    ind = list(random.permutation(N)[:sparsity]);
    x[ind] = matrix(factor*random.random(sparsity) + b).T;
    return x;

########################### Support functions ##############################

def argmax_legal(v, S ):
    """
    Find the maximum index of the column vector v, whose index is not among 
    the indices of S;
    """
    [M,N] = v.shape;
    j = argmax(v);
    if j not in S:
        return j;
    j = 0;
    max_val = v.min();
    for i in xrange(N):
        if max_val <= v[0,i] and i not in S:
                max_val = v[0,i];
                j = i;
    return j

def supp(x, eps=1e-8):
    """
    Find the index of all the nonzero entries of x, and return them as a list.
    An entry is considered nonzero if abs(x[i]) > eps;
    >>>supp(array([2., 0., 0., -4., 0.]))
    [0, 3]
    """
    S = [];
    y = abs(x);
    for i in xrange(len(x)):
        if y[i] > eps:
            S.append(i);
    return S;


# This corresponds to the theoretical L_s function 
def find_index_of_max_abs_vals(x, s): # L
    """Find the index of s largest absolute entries of the array x"""
    x = abs(x);
    # Brute force
    S = [];
    for i in xrange(s): # This search algorithm could be improved
        S.append(argmax(x));
        x[S[-1]] = 0.0;
    return S;

# This corresponds to the theoretical H_s operator 
def hard_thresholding_oper(x,s): # H
    """Set all of the values in x to zero, except the s largest."""    
    L = lambda x,s:  find_index_of_max_abs_vals(x, s);
    S = L(x,s);
    S.sort();
    k = 0;
    for i in xrange(len(x)):
        if i == S[k]:
            k += 1;
            if k == len(S):
                break; # I could write this as a while-loop 
        else:
            x[i] = 0.0;
    
    x[(i+1):len(x)] = 0.0;
    
    return x;

# Created for debugging purposes
def print_arrays(x,y):
    """ 
    Print two arrays side by side
    """
    for i,j in zip(x,y):
        print "%15g, %15g" % (i,j);
    return;
    
########################### Algorithm implementations #######################

def OMP(A, b, eps=1e-3, max_itr=100):
    """Orthogonal Matching Pursuit"""
    [M,N] = A.shape;
    if max_itr > N:
        max_itr = N;
    S = max_itr*[0];
    x = matrix(zeros([N,1]));
    i = 0;
    while i < max_itr and linalg.norm(A*x - b, 1) > eps :
        S[i] = argmax_legal(abs( A.getH()*(b - A*x) ), S);
        
        # Compute the least square solution 
        [ z, residual, rank_n, singular_values ] = linalg.lstsq(A[:, S[:i+1]] , b);    
        x[S[:i+1]] = matrix(z);
        i += 1;
    return [x, i];

def CoSaMP(A, b, sparsity, eps=1e-3, max_itr=100): 
    """Compressive Sampling Matching Pursuit"""
    
    L = lambda x,s: find_index_of_max_abs_vals(x, s);
    H = lambda x,s: hard_thresholding_oper(x,s); 
     
    [M,N] = A.shape;
     
    x = matrix(zeros([N,1]));
    i = 0;
    while i < max_itr and linalg.norm(A*x - b, 1) > eps :
        U = supp(x) + L( A.getH()*(b - A*x), 2*sparsity);
        U.sort();
        
        # Compute the least square solution 
        [u, residual, rank_n, singular_values ] = linalg.lstsq(A[:, U] , b);   
        x = matrix(zeros([N,1]));
        x[U] = x[U] + u;
        x = H(x, sparsity);
        i += 1;
    return [x, i];

def basic_thresholding(A, b, sparsity):
    """Basic Thresholding"""
    L = lambda x,s:  find_index_of_max_abs_vals(x, s);
    S = L(A.getH()*b, sparsity);
    # Compute the least square solution 
    [u, residual, rank_n, singular_values ] = linalg.lstsq(A[:, S] , b);   
    x = matrix(zeros([A.shape[1],1]));
    x[S] = x[S] + u;
    return x, 1; 

def IHT(A,b,sparsity, eps=1e-3, max_itr=100): # Iterative Hard Thresholding
    """Iterative Hard Thresholding"""
    H = lambda x,s:  hard_thresholding_oper(x,s); 
    
    [M, N] = A.shape;    
    x = matrix(zeros([N,1]));
    i = 0;
    while i < max_itr and linalg.norm(A*x - b, 1) > eps :
        x = H( x + A.getH()*(b - A*x) , sparsity );
        i += 1;
    return [x, i];
    
def HTP(A, b, sparsity, eps=1e-3, max_itr=100): # Hard Thresholding pursuit
    """Hard Thresholding Pursuit"""
    # A possible stopping criterion is if S^n == S^{n+1}
    # No guarantee that this will occure
    L = lambda x,s:  find_index_of_max_abs_vals(x, s);
    
    [M, N] = A.shape;    
    x = matrix(zeros([N,1]));
    i = 0;
    
    while i < max_itr and linalg.norm(A*x - b, 1) > eps :
        
        S = L(x + A.getH()*(b-A*x), sparsity);
        # Compute the least square solution 
        [u, residual, rank_n, singular_values ] = linalg.lstsq(A[:, S] , b);   
        x = matrix(zeros([N,1]));
        x[S] = x[S] + u;
        
        i += 1;
        
    return [x, i];
    
    


if __name__ == "__main__":
    #random.seed(4)
    N = 35;
    m = 16;
    s = 2;
    eps = 1e-3;
    num_of_tests = 150;
    max_itr = 2*N;

    print "RECOVERY OF SPARSE SOLUTION TEST";
    print "Unknowns: %d, measurements: %d, sparsity level %d" % (N, m, s);
    print "success level ||Ax-b||_1 < %1.2e, number of tests: %d" % (eps, num_of_tests);
    print "Maximum number of iterations: %d" % max_itr;
    
    OMPitr       = zeros(num_of_tests, dtype=int32); # Number of iterations during a 
    CoSaMPitr    = zeros(num_of_tests, dtype=int32); # successful recovery 
    basicThritr  = zeros(num_of_tests, dtype=int32);
    IHTitr       = zeros(num_of_tests, dtype=int32);
    HTPitr       = zeros(num_of_tests, dtype=int32);
    
    for i in xrange(num_of_tests):
        
        A = create_sampling_mat(m, N);
        x = rand_spars_vec(N, s, factor=10, b= 50);
        b = A*x;
        
        # This would be a perfect point for threading

        # OMP   
        [z, itr] = OMP(A,b, eps, max_itr);
        if (linalg.norm(z-x, 1) < eps):
            OMPitr[i] = itr;
        
        # CoSaMP   
        [z, itr] = CoSaMP(A,b, s, eps, max_itr);
        if (linalg.norm(z-x, 1) < eps):
            CoSaMPitr[i] = itr;
            
        # Basic Thresholding   
        z, itr = basic_thresholding(A, b, s);
        if (linalg.norm(z-x, 1) < eps):
            basicThritr[i] = 1;
        
        # IHT   
        [z, itr] = IHT(A,b, s, eps, max_itr);
        if (linalg.norm(z-x, 1) < eps):
            IHTitr[i] = itr;
        
        # HTP   
        [z, itr] = HTP(A,b, s, eps, max_itr);
        if (linalg.norm(z-x, 1) < eps):
            HTPitr[i] = itr;
       
    totOMPitr    = sum(OMPitr); # total number of successful iterations
    totCoSaMPitr = sum(CoSaMPitr); 
    
    totIHTitr    = sum(IHTitr); 
    totHTPitr    = sum(HTPitr);
    
    OMPsuc         = sum(OMPitr >= 1);
    CoSaMPsuc      = sum(CoSaMPitr >= 1);
    basicThritrsuc = sum(basicThritr >= 1);
    IHTsuc         = sum(IHTitr >= 1);
    HTPsuc         = sum(HTPitr >= 1);    
    
    print "\n%-19s  %s %s" %("METHOD","SUCCESS", "Average number of success iterations");
    
    algNames = ["OMP", "CoSaMP", "Basic Thresholding", "IHT", "HTP"];
    numberOfSuccesses = [OMPsuc, CoSaMPsuc, basicThritrsuc, IHTsuc, HTPsuc];
    itrPerSuc = [totOMPitr, totCoSaMPitr, 1, totIHTitr, totHTPitr];
    
    for algName, numSuc, itrSuc in zip(algNames, numberOfSuccesses, itrPerSuc):
        if numSuc != 0 and algName != "Basic Thresholding":
            print "%-19s: %5.2f%%   %2.2f" % (algName, 100*(float(numSuc)/num_of_tests), float(itrSuc)/numSuc)
        else:
            print "%-19s: %5.2f%%   " % (algName, 100*(float(numSuc)/num_of_tests))

